Ansible Role SNMPd
=========

Install and configure `snmpd`.

Role Variables
--------------

Enable or disable `snmpd` via `snmpd_enable` variable.

The snmpd agent will listen on the interface defined by `snmpd_agentAddress`;
default value is `udp:161`.

The SNMP community and the network from which it's allowed is set by adding
entries to `snmpd_conf_lines` list. But any configuration can be added to this
`snmpd_conf_lines`.

Example Playbooks
-----------------

Access limited to only a single monitoring server:

```yaml
- hosts: servers
  roles:
  - role: ansible-role-snmpd
    snmpd_conf_lines:
    - rocommunity public 10.11.12.13/32
```

Disabling SNMPd :

```yaml
- hosts: servers
  roles:
  - { role: ansible-role-snmpd, snmpd_enable: no }
```

License
-------

GPLv3
